# Northeastern University Libraries - DevOps Project

## Components

1. [Drupal](http://drupal.org)
2. [Drush](http://drupal.org/project/drush)
3. [Git](http://git-scm.com/)
4. [Bitbucket Team Git Repositories](https://bitbucket.org/nu_lts)
5. [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
6. [Vagrant](http://vagrantup.com/)
7. [Puppet](http://www.puppetlabs.com/)
8. [Drush Vagrant Integration](https://drupal.org/project/drush-vagrant)
9. [Drupal-up](https://drupal.org/project/drupal-up)
10. [Aegir](http://www.aegirproject.org/)
10. [Aegir-up](https://drupal.org/project/aegir-up)

## Humans

1. [sbassett](https://bitbucket.org/sbassett)
2. [kyee](https://bitbucket.org/KarlYee)

## Resources

1. Videos
	1. [Drupal Camp Montreal 2012 - Fearless development with Drush, Vagrant and Aegir on Vimeo](http://vimeo.com/51966643)
2. Slideshows

## Diagram

[View/Comment on Google Docs](https://docs.google.com/drawings/d/1vWJ_5t4CKQ-Bf7L5wDlIcUl2g4BHFf0rBQ8TAqKQ3NE/edit)