Fearless development
====================

.. class:: subtitle

with Drush, Vagrant and Aegir

.. include:: <s5defs.txt>

.. class:: presenter

*Presented by*: Christopher Gervais (ergonlogic)


Agenda
------
* What's this got to do with DevOps?
* Why 'Fearless'?
* What are Drush, Vagrant & Aegir?
* Resources

What's this got to do with DevOps?
----------------------------------

.. class:: list-header

Among other things, DevOps is about:

 * Culture of collaboration
 * Infrastructure as code
 * Automating all the things
 * Continuous Deployment


Why 'Fearless'?
---------------

.. class:: list-header

Rules of Ops:

 1. You don't develop in prod!

   ...

Why 'Fearless'?
---------------

.. class:: list-header

Rules of Ops:

 1. You don't develop in prod!
 2. You don't develop in prod!

   ...

Why 'Fearless'?
---------------

.. class:: list-header

Rules of Ops:

 1. You don't develop in prod!
 2. You don't develop in prod!

   ...

.. class:: rulesn

 n. You don't develop in prod!

Why 'Fearless'?
---------------

.. class:: list-header

Setting up a proper development environment is:

 * difficult
 * time-consuming
 * un(der)-appreciated


Why 'Fearless'?
---------------

.. image:: ui/DCMunich_2012/larry_wall.jpg
   :align: right

.. class:: big

"... the three great virtues of a programmer:
      laziness, impatience, and hubris."

.. class:: right

-- *Larry Wall, et. al.*       
   Programming Perl, 2nd Ed.


Why 'Fearless'?
---------------
.. sidebar:: But Devs are:

   * Lazy
   * Impatient
   * Vain

.. class:: list-header

Setting up a dev env is:

 * difficult
 * time-consuming
 * un(der)-appreciated

Tools that can help
-------------------

* Drush *+* Vagrant *=* Drush Vagrant Integration
* Drush-vagrant *+* Aegir *=* Aegir-up

What is Drush?
--------------
.. image:: ui/DCMunich_2012/drush-logo-black.png
   :align: right

* Command line shell and Unix scripting interface for Drupal
* Common and widely supported Drupal power tool
* Written in PHP
* Drupal-specific

What is Vagrant?
----------------
.. image:: ui/DCMunich_2012/vagrant.png
   :align: right

* Command line shell and Unix scripting interface for VirtualBox
* Increasingly popular in DevOps circles
* Written in Ruby

What is Drush-Vagrant?
----------------------
.. image:: ui/DCMunich_2012/drush-vagrant.png
   :align: right

* A Drush extension that provides aliases and useful commands around Vagrant
* Written in PHP (mostly)
* Not exclusively Drupal-specific

What is Aegir?
--------------
.. image:: ui/DCMunich_2012/aegir_logo_smaller.png
   :align: right

* Drush & Drupal
* Aegir makes it easy to *install*, *upgrade*, *deploy*, and *backup* an entire network of Drupal sites
* Written in PHP (mostly)

What is Aegir-up?
-----------------
.. image:: ui/DCMunich_2012/aegir-up.png
   :align: right

* Aegir-up is a Drush-vagrant blueprint
* Local Aegir VM
* Written in PHP & Puppet

Why Fearless?
-------------

.. sidebar:: Dev envs *were*:

 * difficult
 * time-consuming
 * un(der)-appreciated

.. class:: list-header

DevOps is about:

* Culture of collaboration
* Infrastructure as code
* Automating all the things
* Continuous Deployment

Why Fearless?
-------------

* Rebuild your entire environment quickly and easily
* Avoid 'works in dev, broken in prod'
* Share environments within and between Dev and Ops teams
* Simplifies the automation of building test environments

Future directions
-----------------

* Test patches easily (drush --iq)
* Continuous Integration (Jenkins) VM
* End-to-end workflow automation

Resources
---------
 * http://drush.org
 * http://vagrantup.org
 * http://drupal.org/project/drush-vagrant
 * http://community.aegirproject.org
 * http://drupal.org/project/aegir-up

Questions?
-------------------

* Got suggestions?
* How will *you* use Drush, Vagrant and Aegir?

.. class:: big

**THANK YOU!**
